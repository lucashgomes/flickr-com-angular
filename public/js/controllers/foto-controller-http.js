angular.module("flickr")
    .controller("FotoController", function ($scope, recursoFoto, $routeParams) {

        $scope.foto = {};
        $scope.mensagem = "";

        if ($routeParams.fotoId) {
            $http.get("/v1/fotos/" + $routeParams.fotoId)
                .success(function (foto) {
                    $scope.foto = foto;
                    console.log("Exibindo foto");
                })
                .error(function (erro) {
                    $scope.mensagem = "Erro ao exibir foto";
                });
        }

        $scope.submeter = function () {
            //Se o formulário é válido(com todas as informações requeridas preenchidas)
            if ($scope.formulario.$valid) {
                //se já possui id então o user quer fazer uma alteração
                if ($scope.foto._id) {
                    $http.put("/v1/fotos/" + $scope.foto._id, $scope.foto)
                        .success(function () {
                            $scope.mensagem = "Foto " + $scope.foto.titulo + " alterada com sucesso!";
                        })
                        .error(function (erro) {
                            $scope.mensagem = "Erro ao alterar a foto " + $scope.foto.titulo;
                        });
                } else {
                    //envia foto para o servidor pelo metodo post e com o serviço http
                    $http.post("/v1/fotos", $scope.foto)
                        .success(function () {
                            $scope.foto = {}; //Limpa formulario
                            $scope.mensagem = "Foto salva com sucesso!";
                        })
                        .error(function (erro) {
                            $scope.mensagem = "Erro ao salvar a foto: " + erro;
                        });
                }

            }

        };

    });