angular.module("flickr")
    .controller("FotoController", function ($scope, recursoFoto, $routeParams) {

        $scope.foto = {};
        $scope.mensagem = "";

        if ($routeParams.fotoId) {
            recursoFoto.get({
                fotoId: $routeParams.fotoId
            }, function (foto) {
                $scope.foto = foto;
                console.log("Exibindo foto");
            }, function (error) {
                $scope.mensagem = "Erro ao exibir foto";
            });
        }

        $scope.submeter = function () {
            //Se o formulário é válido(com todas as informações requeridas preenchidas)
            if ($scope.formulario.$valid) {
                //se já possui id então o user quer fazer uma alteração
                if ($scope.foto._id) {
                    recursoFoto.update({
                        fotoId: $scope.foto._id
                    }, $scope.foto, function () {
                        $scope.mensagem = "Foto " + $scope.foto.titulo + " alterada com sucesso!";
                    }, function (erro) {
                        $scope.mensagem = "Erro ao alterar a foto " + $scope.foto.titulo;
                    });
                } else {
                    recursoFoto.save($scope.foto, function () {
                        $scope.foto = {};
                        $scope.mensagem = "Foto salva com sucesso!";
                    }, function (erro) {
                        $scope.mensagem = "Erro ao salvar a foto: " + erro;
                    });
                }

            }

        };

    });