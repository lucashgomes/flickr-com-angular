angular.module("flickr")
    .controller("FotoController", function ($scope, cadastroFoto, recursoFoto, $routeParams) {

        $scope.foto = {};
        $scope.mensagem = "";

        if ($routeParams.fotoId) {
            recursoFoto.get({
                fotoId: $routeParams.fotoId
            }, function (foto) {
                $scope.foto = foto;
                console.log("Exibindo foto");
            }, function (error) {
                $scope.mensagem = "Erro ao exibir foto";
            });
        }

        $scope.submeter = function () {
            //Se o formulário é válido(com todas as informações requeridas preenchidas)
            if ($scope.formulario.$valid) {

                cadastroFoto.cadastrar($scope.foto)
                    .then(function (dados) {
                        $scope.mensagem = dados.mensagem;
                        if (dados.inclusao) $scope.foto = {};
                    })
                    .catch(function (dados) {
                        $scope.mensagem = dados.mensagem;
                    })
            }

        };

    });