angular.module('flickr').controller('FotosController', function ($scope, recursoFoto) {

	$scope.fotos = [];
	$scope.filtro = '';
	$scope.mensagem = '';

	//Recebe duas funcoes anonimas, sendo a primeira para success e a segunda para error
	recursoFoto.query(function (fotos) {
			$scope.fotos = fotos;
		},
		function (erro) {
			console.log("Erro na query" + erro);
		});
	/*
		$http.get('/v1/fotos')
			.success(function (retorno) {
				console.log(retorno);
				$scope.fotos = retorno; // não precisa fazer retorno.data
			})
			.error(function (erro) {
				console.log(erro);
			});
	*/

	$scope.remover = function (foto) {

		recursoFoto.delete({
			fotoId: foto._id
		}, function () {
			var indiceFoto = $scope.fotos.indexOf(foto);
			$scope.fotos.splice(indiceFoto, 1); //remove da lista fotos 1 foto referenciada(via index)
			$scope.mensagem = "Foto " + foto.titulo + " removida com sucesso!";
		}, function (erro) {
			$scope.mensagem = "Erro " + erro + " ao remover a foto " + foto.titulo;
		});
	};

	/*
		$scope.remover = function (foto) {
			$http.delete('/v1/fotos/' + foto._id)
				.success(function () {
					var indiceFoto = $scope.fotos.indexOf(foto);
					$scope.fotos.splice(indiceFoto, 1); //remove da lista fotos 1 foto referenciada(via index)
					$scope.mensagem = "Foto " + foto.titulo + " removida com sucesso!";
				})
				.error(function (erro) {
					$scope.mensagem = "Erro " + erro + " ao remover a foto " + foto.titulo;
				});
		};
	*/
});