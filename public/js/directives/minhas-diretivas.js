angular.module('minhasDiretivas', [])

    .directive('meuPainel', function () {

        var ddo = {};
        //A restrição pode ser utilizada como atributo(A) e elemento(E)
        ddo.restrict = "AE";
        ddo.transclude = true;

        //Quais são os atributos HTML dessa diretiva. O @ representa que uma string será copiada para titulo
        ddo.scope = {
            titulo: '@'
        };
        //templateUrl será utilizado quando não houver um arquivo com o HTML da diretiva
        ddo.templateUrl = 'js/directives/meu-painel.html';

        return ddo;
    })

    .directive('minhaFoto', function () {

        var ddo = {};

        ddo.restrict = "AE";

        ddo.scope = {
            titulo: '@',
            url: '@'
        };

        ddo.template = '<img class="img-responsive center-block" src="{{url}}" alt="{{titulo}}">';

        return ddo;
    })

    .directive('meuBotaoPerigo', function () {

        var ddo = {};

        ddo.restrict = "E";

        ddo.scope = {
            nome: '@', //recebe uma string
            acao: '&' //recebe uma função
        }

        ddo.templateUrl = '<button ng-click="acao()" class="btn btn-danger btn-block m-0 p-0">{{nome}}</button>';

        return ddo;
    })

    .directive('meuFocus', function () {

        var ddo = {};

        ddo.restrict = "A";

        ddo.scope = {
            focado: '=' //Qualquer alteração na propriedade ou diretiva, afeta o controller. TWO-WAY
        }
        /**
         * A propriedade LINK é quem acessa/manipula o DOM e recebe dois parametros em SEQUENCIA: scope e element
         */
        ddo.link = function (scope, element) {
            scope.$on('salvarFoto', function () {
                element[0].focus();
            });
        };

        return ddo;
    });


/* Quando o elemento tem seu estado alterado(e focado == true, neste caso), o $watch dispara um evento
scope.$watch('focado', function () {
    if (scope.focado) {
        element[0].focus();
        scope.focado = false;
    }
});*/