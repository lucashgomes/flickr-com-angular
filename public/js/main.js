angular.module('flickr', ['minhasDiretivas', 'ngAnimate', 'ngRoute', 'meusServicos'])
	.config(function ($routeProvider, $locationProvider) {

		$locationProvider.html5Mode(true);

		$routeProvider.when('/fotos', {
			templateUrl: 'partials/principal.html',
			controller: 'FotosController'
		});

		$routeProvider.when('/fotos/new', {
			templateUrl: 'partials/foto.html',
			controller: 'FotoController'
		});
		//pega via url o id da foto a ser alterada com o curinga :var_index
		$routeProvider.when('/fotos/edit/:fotoId', {
			templateUrl: 'partials/foto.html',
			controller: 'FotoController'
		});

		$routeProvider.otherwise({
			redirectTo: '/fotos'
		});

	});