angular.module('meusServicos', ['ngResource'])
    .factory('recursoFoto', function ($resource) {

        //Já que ngResource não possui o método PUT do $http, esta é a forma de implementá-lo usando $resource 
        return $resource('/v1/fotos/:fotoId', null, {
            update: {
                method: 'PUT'
            }
        });

    })
    //$rootScope é quem abriga todos os scopes utilizados pelos controllers
    .factory('cadastroFoto', function (recursoFoto, $q, $rootScope) {

        var servico = {};
        var event = 'salvarFoto';

        servico.cadastrar = function (foto) {
            //o serviço $q(utilizado para criar promisses) recebe 2 funcoes, 1 de sucesso(resolve) e uma de erro*reject) que serão utilizadas com .then e catch
            return $q(function (resolve, reject) {

                if (foto._id) {

                    recursoFoto.update({
                        fotoId: foto._id
                    }, foto, function () {
                        //Dispara o evento salvarFoto
                        $rootScope.$broadcast(event);

                        resolve({
                            mensagem: "Foto " + foto.titulo + " atualizada com sucesso!",
                            inclusao: false
                        });

                    }, function (erro) {
                        console.log("Erro ao atualizar foto: " + erro);
                        reject({
                            mensagem: "Não foi possível atualizar a foto " + foto.titulo
                        });
                    });
                } else {

                    recursoFoto.save(foto, function () {

                        //Dispara o evento salvarFoto
                        $rootScope.$broadcast(event);

                        resolve({
                            mensagem: "Foto salva com sucesso!",
                            inclusao: true
                        });

                    }, function (erro) {
                        console.log("Erro ao salvar a foto");

                        reject({
                            mensagem: 'Não foi possível salvar a foto ' + foto.titulo
                        });

                    });
                }
            });
        };

        return servico;

    });